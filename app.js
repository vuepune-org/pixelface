new Vue ({
    el: '#app',
    data: {
        appName: 'PixelFace',
        userName: '',
        gender: 'male',
        isClicked: false
    },
    computed: {
        showFace: function() {
            return `https://avatars.dicebear.com/v2/${this.gender}/${this.userName}.svg`;
        }
    },
    methods: {
        showMyFace() {
            this.isClicked = true;
        },
        clearFace() {
            this.isClicked = false;
        }
    }
});